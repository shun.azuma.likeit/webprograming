package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MyUserDAO;
import model.Userbeans;

/**
 * Servlet implementation class Myuserdelete
 */
@WebServlet("/Myuserdelete")
public class Myuserdelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Myuserdelete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Userbeans ses =(Userbeans)session.getAttribute("userInfo");

		if(ses==null) {response.sendRedirect("MyLoginServlet");
		return;
		}



		String id = request.getParameter("id");
		MyUserDAO userDao = new MyUserDAO();
		Userbeans user = userDao.detail(id);

		request.setAttribute("userdetail", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_delete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		MyUserDAO userDao = new MyUserDAO();
        userDao.delete(id);

        response.sendRedirect("MyUserlistServlet");
	}

}
