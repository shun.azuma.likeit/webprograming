package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MyUserDAO;
import model.Userbeans;

/**
 * Servlet implementation class MyUser_register
 */
@WebServlet("/MyUser_register")
public class MyUser_register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUser_register() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Userbeans ses = (Userbeans) session.getAttribute("userInfo");

		if (ses == null) {
			response.sendRedirect("MyLoginServlet");
			return;

		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_register.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordtwo = request.getParameter("passwordtwo");
		String name = request.getParameter("name");
		String date = request.getParameter("date");

		//未入力
		if (loginId.equals("") || password.equals("") || (name.equals("") || date.equals(""))) {

			request.setAttribute("errMsg", "入力された内容はただしくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_register.jsp");
			dispatcher.forward(request, response);
			return;

		}
		//password不一致
		else if (!password.equals(passwordtwo)) {
			request.setAttribute("errMsg", "入力された内容はただしくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_register.jsp");
			dispatcher.forward(request, response);
			return;
		}
		MyUserDAO userDao = new MyUserDAO();

		Userbeans user = userDao.Idserach(loginId);

		/** テーブルに該当のデータが見つかった場合 **/
		if (user != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容はただしくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_register.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//DAO起動

		userDao.insert(loginId, password, name, date);

		//リストに飛ばす
		response.sendRedirect("MyUserlistServlet");

	}

}
