package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MyUserDAO;
import model.Userbeans;

/**
 * Servlet implementation class MyUserlistServlet
 */
@WebServlet("/MyUserlistServlet")
public class MyUserlistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUserlistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Userbeans ses =(Userbeans)session.getAttribute("userInfo");

		if(ses==null) {response.sendRedirect("MyLoginServlet");

		return;
		}

		// ユーザ一覧情報を取得
		MyUserDAO userDao = new MyUserDAO();
		List<Userbeans> userList = userDao.findAllwithout();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String loginIdP = request.getParameter("loginId");
		String nameP = request.getParameter("name");
		String dateP = request.getParameter("date");
		String datePP = request.getParameter("dateend");

		MyUserDAO userDao = new MyUserDAO();

		List<Userbeans> userList= userDao.search(loginIdP,nameP,dateP,datePP);

		request.setAttribute("userList", userList);

		//飛ばす

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		dispatcher.forward(request, response);

	}
}
