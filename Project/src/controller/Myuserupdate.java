package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MyUserDAO;
import model.Userbeans;

/**
 * Servlet implementation class Myuserupdate
 */
@WebServlet("/Myuserupdate")
public class Myuserupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Myuserupdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Userbeans ses =(Userbeans)session.getAttribute("userInfo");

		if(ses==null) {response.sendRedirect("MyLoginServlet");
         return;
		}
		




		String id = request.getParameter("id");
		MyUserDAO userDao = new MyUserDAO();
		Userbeans user = userDao.detail(id);

		request.setAttribute("userdetail", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String password = request.getParameter("password");
		String passwordtwo = request.getParameter("passwordtwo");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("date");
		String id = request.getParameter("id");



		if (name.equals("") || birthDate.equals("")) {

			request.setAttribute("errMsg", "入力された内容はただしくありません");


			MyUserDAO userDao = new MyUserDAO();
			Userbeans user = userDao.detail(id);

			request.setAttribute("userdetail", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;

		}
		else if (!password.equals(passwordtwo)) {
			request.setAttribute("errMsg", "入力された内容はただしくありません");


			MyUserDAO userDao = new MyUserDAO();
			Userbeans user = userDao.detail(id);

			request.setAttribute("userdetail", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		MyUserDAO userDao = new MyUserDAO();

		if(password.equals("")||passwordtwo.equals("")) {
			userDao.Updatetwo(name, birthDate, id);
		}
		else {

		userDao.Update(password, name, birthDate, id);
		}
		response.sendRedirect("MyUserlistServlet");
	}

}
