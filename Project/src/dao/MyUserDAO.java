package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Userbeans;

public class MyUserDAO {

	/**
	* ログインIDとパスワードに紐づくユーザ情報を返す
	* @param loginId
	* @param password
	* @return
	*/
	public Userbeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;

		mdfive pas= new mdfive();
		String mdpassword=pas.five(password);


		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();


			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, mdpassword);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new Userbeans(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<Userbeans> findAll() {
		Connection conn = null;
		List<Userbeans> userList = new ArrayList<Userbeans>();

		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Userbeans user = new Userbeans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}



	public List<Userbeans> findAllwithout() {
		Connection conn = null;
		List<Userbeans> userList = new ArrayList<Userbeans>();

		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where id !=1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Userbeans user = new Userbeans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}





	//新規登録　 SQLにデータいれる　
	public void insert(String loginId, String password, String name, String date) {
		Connection con = null;
		PreparedStatement stmt = null;

		mdfive pas= new mdfive();
		String mdpassword=pas.five(password);


		try {
			// データベース接続
			con = MyDBmanager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);
			stmt.setString(2, mdpassword);
			stmt.setString(3, name);
			stmt.setString(4, date);
			// 登録SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	//詳細ボタン

	public Userbeans detail(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new Userbeans(Id, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}


	public  List<Userbeans>search(String loginIdP,String nameP, String dateP,String datePP) {

		Connection conn = null;
		List<Userbeans> userList = new ArrayList<Userbeans>();

		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where id !=1";

			if(!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";
			}

			if(!nameP.equals("")) {
				sql += " AND name like '"+"%" + nameP+"%" + "'";

			}

			if(!dateP.equals("")) {
				sql += " AND '" +dateP+"'"+"<= birth_date";
			}


			if(!datePP.equals("")) {
				sql += " AND '"+datePP+"'"+">=birth_date";

			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Userbeans user = new Userbeans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;


	}


	/*
	public  List<Userbeans>search(String Name) {
		Connection conn = null;
		List<Userbeans> userList = new ArrayList<Userbeans>();
		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE name like ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%"+Name+"%");
			ResultSet rs = pStmt.executeQuery();


			while(rs.next()) {

			// 必要なデータのみインスタンスのフィールドに追加
			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			Userbeans user=new Userbeans(Id, loginId, name, birthDate, password, createDate, updateDate);

			userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
	*/







	//更新ボタン
	public void Update(String password, String name, String date, String id) {
		Connection con = null;
		PreparedStatement stmt = null;

		mdfive pas= new mdfive();
		String mdpassword=pas.five(password);


		try {
			// データベース接続
			con = MyDBmanager.getConnection();
			// 実行SQL文字列定義 パスワード　生年月日　ユーザー名　を更新　ログインIDはそのまま
			String updateSQL = "update user set password=?, name=?, birth_date=? where id=?";
			// ステートメント生成
			stmt = con.prepareStatement(updateSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, mdpassword);
			stmt.setString(2, name);
			stmt.setString(3, date);
			stmt.setString(4, id);
			// 更新する
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	//削除ボタン
	public void delete(String id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = MyDBmanager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "delete from user where id=?";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, id);
			//削除実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public Userbeans Idserach(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = MyDBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");

			return new Userbeans(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//更新　パスワード空欄ver
	public void Updatetwo(String name, String birthDate, String id) {
		// TODO 自動生成されたメソッド・スタブ

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = MyDBmanager.getConnection();
			// 実行SQL文字列定義 パスワード　生年月日　ユーザー名　を更新　ログインIDはそのまま
			String updateSQL = "update user set name=?, birth_date=? where id=?";
			// ステートメント生成
			stmt = con.prepareStatement(updateSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3, id);
			// 更新する
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
