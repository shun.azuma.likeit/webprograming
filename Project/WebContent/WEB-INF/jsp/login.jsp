<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col align-self-center">
				<h1 class="text-center">ログイン画面</h1>
			</div>
		</div>
	</div>
	<form class="form-signin" action="MyLoginServlet" method="post">
		<div class="container">
			<c:if test="${errMsg != null}">

				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<div class="row">
				<div class="col align-self-center">
					<p class="text-center">
						ログインID<input type="text" name="loginId">
					</p>
					<p class="text-center">
						パスワード<input type="password" name="password">
					</p>
				</div>
			</div>
		</div>
		<p class="text-center">
			<input type="submit" value="ログイン">
		</p>
	</form>
</body>
</html>