<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userlist</title>
    <link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"crossorigin="anonymous">

</head>

<body>

     <nav class="navbar navbar-expand-sm navbar-dark bg-dark mt-3 mb-3">
       <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                 <p class="text-white bg-dark">${userInfo.name}</p>
                </li>
                <li class="nav-item">
                    <a class="text-danger" href="">ログアウト</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <div class="col align-self-center">
    <h1 class="text-center">ユーザー情報更新</h1>
    </div>

    </div>



    <div class="container">
  <div class="row">
    <div class="col">
    <p>ログインID</p>
    </div>
    <div class="col">
    <p>${userdetail.loginId}</p>
    </div>
  </div>
</div>

  <form class="form-update" action="Myuserupdate" method="post">

  <input type="hidden" name="id" value="${userdetail.id}">
<div class="container">

 <c:if test="${errMsg != null}">

		<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

  <div class="row">
    <div class="col">
   <p>パスワード</p>
    </div>
    <div class="col">
      <input type="text" name="password">
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col">
   <p>パスワード（確認）</p>
    </div>
    <div class="col">
      <input type="text"name="passwordtwo">
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col">
   <p>ユーザー名</p>
    </div>
    <div class="col">
      <input type="text" value="${userdetail.name }" name="name">
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col">
   <p>生年月日</p>
    </div>
    <div class="col">
      <input type="date" value="${userdetail.birthDate}" name="date">
    </div>
  </div>
</div>

<p class="text-center"><input type="submit" value="更新"></p>
 </form>
 <a class="nav-link" href="MyUserlist">戻る</a>
</body>
</html>