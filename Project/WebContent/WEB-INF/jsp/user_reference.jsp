<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userlist</title>
    <link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"crossorigin="anonymous">

</head>

<body>

       <nav class="navbar navbar-expand-sm navbar-dark bg-dark mt-3 mb-3">
       <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                 <p class="text-white bg-dark">${userInfo.name}</p>
                </li>
                <li class="nav-item">
                    <a class="text-danger" href="#">ログアウト</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
         <div class="col align-self-center">
    <h1 class="text-center">ユーザー情報詳細参照</h1>
        </div>



    <div class="container">
  <div class="row">
    <div class="col">
    <p>${userdetail.loginId}ログインID</p>
    </div>
    <div class="col">
  <p>${userdetail.loginId}</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col">
   <p>ユーザー名</p>
    </div>
    <div class="col">
    <p>${userdetail.name}</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col">
   <p>生年月日</p>
    </div>
    <div class="col">
     <p>${userdetail.birthDate}</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col">
   <p>登録日時</p>
    </div>
    <div class="col">
   <p> ${userdetail.createDate}</p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col">
   <p>更新日時</p>
    </div>
    <div class="col">
     <p> ${userdetail.updateDate}</p>
    </div>
  </div>
</div>
<a class="nav-link" href="MyUserlistServlet">戻る</a>


</body>
</html>