<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userlist</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>

<body>

	<nav class="navbar navbar-expand-sm navbar-dark bg-dark mt-3 mb-3">
		<div class="collapse navbar-collapse justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-item">
					<p class="text-white bg-dark">${userInfo.name}</p>
				</li>
				<li class="nav-item"><a class="text-danger" href="logout">ログアウト</a>
				</li>
			</ul>
		</div>
	</nav>
<form class="search" action="MyUserlistServlet" method="post">
	<div class="container">

		<div class="col align-self-center">
			<h1 class="text-center">ユーザ一覧</h1>
		</div>
	</div>
	<div align="right">
		<a class="nav-link" href="MyUser_register">新規登録</a>
	</div>
	<div class="text-center">
		<div class="container">
			<div class="row">
				<div class="col">
					<p>ログインID</p>
				</div>
				<div class="col">
					<input type="text" name="loginId">
				</div>
				<div class="col"></div>
			</div>
		</div>


		<div class="container">
			<div class="row">
				<div class="col">
					<p>ユーザー名</p>
				</div>
				<div class="col">
					<input type="text"name="name">
				</div>
				<div class="col"></div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col">
					<p>生年月日</p>
				</div>
				<div class="col" style="display: inline-flex">
					<input type="date"name="date">
					<p>～</p>
					<input type="date"name="dateend">
				</div>
				<div class="col"></div>
			</div>
		</div>
	</div>


	<p class="text-right">
		<input type="submit" style="width: 100px" value="検索">
	</p>
    </form>

	<table class="table table-bordered">

		<thead>
			<tr class="text-center">
				<th scope="col">ログインID</th>
				<th scope="col">ユーザー名</th>
				<th scope="col">生年月日</th>
				<th scope="col"></th>
			</tr>

		</thead>

		<tbody>
			<c:choose>
			<c:when test="${userInfo.loginId== 'admin' }">
					<c:forEach var="user" items="${userList}">
						<tr>
							<th scope="row">${user.loginId}</th>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>

							<td class="text-center"><a class="btn btn-primary"
								href="Myuserdetail?id=${user.id}">詳細</a> <a
								class="btn btn-success" href="Myuserupdate?id=${user.id}">更新</a>
								<a class="btn btn-danger" href="Myuserdelete?id=${user.id}">削除</a>
							</td>
						</tr>
					</c:forEach>
					</c:when>
					<c:when test="${userInfo.loginId!='admin'}">
						<c:forEach var="user" items="${userList}">
							<tr>
								<th scope="row">${user.loginId}</th>
								<td>${user.name}</td>
								<td>${user.birthDate}</td>
                                <c:if test="${user.loginId==userInfo.loginId}" >
								<td class="text-center"><a class="btn btn-primary" href="Myuserdetail?id=${user.id}">詳細</a>

							    <a class="btn btn-success" href="Myuserupdate?id=${user.id}">更新</a>
								</c:if>

								<c:if test="${user.loginId !=userInfo.loginId}" >
								<td class="text-center"><a class="btn btn-primary" href="Myuserdetail?id=${user.id}">詳細</a>
								</td>
								</c:if>

							</tr>
						</c:forEach>

					</c:when>

		</c:choose>
		</tbody>
	</table>
</body>
</html>